$( document ).ready(function() {
    $(".can-focus").keypress(function (e) {
        if (e.keyCode == 32 || e.keyCode == 13 || e.charCode == 32 || e.charCode == 13) {
            $(this).trigger('click');
        }
    });

    $('.back-to-top').click(function(){
        $('html, body').animate({scrollTop : 0},1500, 'easeInOutExpo', function() {
            $('#content').focus();
          });

        return false;
    });

});
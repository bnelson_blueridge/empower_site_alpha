function duplicates(){
//search for duplicates
var ids = [], // List of known IDs
            dupes = [],
            $list = $('#duplicate');

        $('body').find('[id]').each(function() {
            if (this.id) {
              if (!/^[A-Za-z]/.test(this.id)) {
                console.log('<li>The ID <code>#' + this.id + '</code> is not valid; it must begin with a letter</li>');
              }
              else {
                ids.push(this.id);
              }
            }
        });
var counting = {};
ids.forEach(function (str) {
    counting[str] = (counting[str] || 0) + 1;
});

if (Object.keys(counting).length !== ids.length) {
    console.log("Has duplicates");

    var str;
    for (str in counting) {
        if (counting.hasOwnProperty(str)) {
            if (counting[str] > 1) {
                console.log(str + " appears " + counting[str] + " times");
            }
        }
    }
}}
duplicates();

function altFind() {
    $('img').each(function(){
        $(this).wrap('<span></span>');
        var mySrc = $(this).attr('src');
        var html = $(this).parent().html();
        var alt = html.match( /alt=/ );
        if(alt==null) {
            if (mySrc == 'images/icons/acp.png' || mySrc == 'images/icons/resources.png' || mySrc == 'images/icons/log.png'|| mySrc == 'images/icons/back.png'|| mySrc == 'images/icons/next.png') {
                console.log(mySrc)
            } else {
                console.log('THERE IS NO ALT ATTRIBUTE ' + mySrc);
            }
        } else {
            if($(this).attr('alt')) {
                console.log('ALT ATTRIBUTE IS FILLED! yay!');
            } else {
                console.log('ALT ATTRIBUTE IS EMPTY');
            }
        }
    });
}
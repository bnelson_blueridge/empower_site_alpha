import { PluginDef } from '@fullcalendar/common';
import '@fullcalendar/premium-common';

declare const _default: PluginDef;

export default _default;
